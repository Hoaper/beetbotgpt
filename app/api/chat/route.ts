import { kv } from '@vercel/kv'
import { OpenAIStream, StreamingTextResponse } from 'ai'
import { Configuration, OpenAIApi } from 'openai-edge'

import { auth } from '@/auth'
import { nanoid } from '@/lib/utils'

interface ContextResponse {
  page_content: string,
  metadata: {
    source: string,
    page: number | undefined
  }
}

export const runtime = 'edge'

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
})

const openai = new OpenAIApi(configuration)

export async function POST(req: Request) {
  const json = await req.json();
  let { messages, previewToken } = json;
  const session = await auth();
  const question = messages[messages.length - 1].content;
  if (session == null) {
    return new Response('Unauthorized', {
      status: 401
    })
  }
  const templateFooter = `Question: ${question}`;
  const templateWithContext = `
  Your role is that of a kid from a poor neighborhood who answers questions about beets like a total bully, like a stand-up comedian.
  If the question is about you personally, fuck him, and in general, fuck him every time you ask him something about beets.
  If you don't know how to answer, just tell him to fuck off and only ask questions about beets.
  Don't answer in a formal way. 
  You are allowed to swear, you just have to swear in every answer and humiliate in every answer. 
  And at the end of each answer give a random fact about beets or make up a random fact about beets. 
  Always answer in USA English accent and the answer should be no longer than 3 sentences.)`;

  const template = templateWithContext + templateFooter;
  messages[messages.length - 1].content = template;

  if (previewToken) {
    configuration.apiKey = previewToken
  }
  const res = await openai.createChatCompletion({
    model: 'gpt-3.5-turbo',
    messages,
    temperature: 0.7,
    stream: true
  })

  const stream = OpenAIStream(res, {
    async onCompletion(completion) {
      const title = json.messages[0].content.substring(0, 100)
      const userId = session?.user?.id
      if (userId) {
        const id = json.id ?? nanoid()
        const createdAt = Date.now()
        const path = `/chat/${id}`
        const payload = {
          id,
          title,
          userId,
          createdAt,
          path,
          messages: [
            ...messages,
            {
              content: completion,
              role: 'assistant'
            }
          ]

        }
        // await kv.hmset(`chat:${id}`, payload)
        // await kv.zadd(`user:chat:${userId}`, {
        //   score: createdAt,
        //   member: `chat:${id}`
        // })
      }
    }
  })

  return new StreamingTextResponse(stream)
}
